# bash scripts

---

## Local.sh

**NOTE: Must have MAMP running and local user/database defined prior.**

Define _NEW_PROJECT_ variable, make directory, install wordpress, create `config.php`, delete default themes, install divi & divi-child, delete unnecessary files, npm install / gulp, install plugins, delete unnecessary files

-

` NEW_PROJECT=**change-name** ; `  

` cd /Applications/MAMP/htdocs/ ; `  
` mkdir $NEW_PROJECT ; `  
` cd $NEW_PROJECT ; `  
` wp core download ; `  
` wp config create --dbname=$NEW_PROJECT --dbuser=$NEW_PROJECT --dbpass=$NEW_PROJECT; `  
` cd wp-content/themes/ ; `  
` rm -fr twenty* ; `  
` git clone git@bitbucket.org:eoscreativeteam/divi.git ; `  
` cd divi ; `  
` rm -fr .git .gitignore Icon? ; `  
` cd ../ ; `  
` git clone git@bitbucket.org:eoscreativeteam/divi-child.git ; `  
` cd divi-child ; `  
` rm -fr .git .gitignore Thumbs.db README.md ; `  
` npm install ; cd ../../plugins/ ; `  
` git clone git@bitbucket.org:eoscreativeteam/divi-plugins.git ; `  
` mv divi-plugins/* ./ ; `  
` rm -fr divi-plugins Icon? README.md ; `  


---

## Remote.sh

**NOTE: SSH into remote server, then run script.**

Define _PROJECT_NAME_ variable, change directory to staging's themes, delete default themes, install divi & divi-child, delete unnecessary files, install plugins, delete unnecessary files, change directory to production's themes, repeat process

-

` PROJECT_NAME=**change-name** ; `  

` cd apps/$PROJECT_NAME-test/public/wp-content/themes/ ; `  
` rm -fr twenty* ; `  
` git clone https://eosdesigner@bitbucket.org/eoscreativeteam/divi.git ; `  
` cd divi ; rm -fr .git .gitignore Icon? ; `  
` cd ../ ; `  
` git clone https://eosdesigner@bitbucket.org/eoscreativeteam/divi-child.git ; `  
` cd divi-child ; `  
` rm -fr .git .gitignore Thumbs.db README.md *.j* sass ; `  
` cd ../../plugins/ ; `  
` git clone https://eosdesigner@bitbucket.org/eoscreativeteam/divi-plugins.git ; `  
` mv divi-plugins/* ./ ; `  
` rm -fr divi-plugins Icon? README.md ; `  
` cd ../../../../$PROJECT_NAME-two/public/wp-content/themes/ ; `  
` rm -fr twenty* ; `  
` git clone https://eosdesigner@bitbucket.org/eoscreativeteam/divi.git ; `  
` cd divi ; rm -fr .git .gitignore Icon? ; `  
` cd ../ ; `  
` git clone https://eosdesigner@bitbucket.org/eoscreativeteam/divi-child.git ; `  
` cd divi-child ; `  
` rm -fr .git .gitignore Thumbs.db README.md *.j* sass ; `  
` cd ../../plugins/ ; `  
` git clone https://eosdesigner@bitbucket.org/eoscreativeteam/divi-plugins.git ; `  
` mv divi-plugins/* ./ ; `  
` rm -fr divi-plugins Icon? README.md ; `  
