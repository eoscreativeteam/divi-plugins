<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://diviwebdesign.com/
 * @since      1.0.0
 *
 * @package    Divi_Tweaker
 * @subpackage Divi_Tweaker/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Divi_Tweaker
 * @subpackage Divi_Tweaker/includes
 * @author     Divi Web Design <hello@diviwebdesign.com>
 */
class Divi_Tweaker_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'divi-tweaker',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
