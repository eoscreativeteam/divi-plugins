<?php

/**
 * WordPress settings API demo class
 *
 * @author Tareq Hasan
 */
if ( !class_exists('DWD_Tweaker_Settings_Page' ) ):
class DWD_Tweaker_Settings_Page {

    private $settings_api;

    function __construct() {
        $this->settings_api = new DWD_Tweaker_Settings_API;

        add_action( 'admin_init', array($this, 'admin_init') );
        add_action( 'admin_menu', array($this, 'admin_menu') );
    }

    function admin_init() {

        //set the settings
        $this->settings_api->set_sections( $this->get_settings_sections() );
        $this->settings_api->set_fields( $this->get_settings_fields() );

        //initialize settings
        $this->settings_api->admin_init();
    }

    function admin_menu() {
        add_options_page( 'Divi Tweaker', 'Divi Tweaker', 'delete_posts', 'divi_tweaker', array($this, 'plugin_page') );
    }

    function get_settings_sections() {
        $sections = array(
            array(
                'id'    => 'dwd_tweaker_speed',
                'title' => __( 'Speed Improvements', 'dwdtweaker' ),
                'desc'  => __( 'Here are some Divi Speed Settings that will help your Divi fly high.', 'dwdtweaker' ),
            ),
            array(
                'id'    => 'dwd_tweaker_page_transition',
                'title' => __( 'Page Transition', 'dwdtweaker' ),
                'desc'  => __( 'This Page Transition is using jquery animsition plugin by Bilvesta. Please check demo at <a href="http://git.blivesta.com/animsition/" taget="_blank">http://git.blivesta.com/animsition/</a> for now', 'dwdtweaker' ),
            ),
            array(
                'id'    => 'dwd_tweaker_design',
                'title' => __( 'Design Tweaks', 'dwdtweaker' ),
                'desc'  => __( 'Enhance your Divi design experiences without coding.', 'dwdtweaker' ),
            ),
            array(
                'id'    => 'dwd_tweaker_advanced',
                'title' => __( 'Advanced Tweaks', 'dwdtweaker' ),
                //'desc'  => __( 'Should use with care even though it is tested.', 'dwdtweaker' ),
            )
        );
        return $sections;
    }

    /**
     * Returns all the settings fields
     *
     * @return array settings fields
     */

    function get_settings_fields() {
        $settings_fields = array(
            'dwd_tweaker_speed' => array(
                array(
                    'name'    => 'divi_speedup',
                    'label'   => __( 'Speed Up Divi', 'dwdtweaker' ),
                    'desc'    => __( 'Remove Divi CSS & Scripts unless the user is using Divi Builder. Thanks to <a href="https://adamhaworth.com/dequeue-divi-builder-plugin-scripts-and-styles-when-not-needed/" target="_blank">Adam Haworth</a> for function script.', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'woocommerce_speedup',
                    'label'   => __( 'Speed Up WooCommerce', 'dwdtweaker' ),
                    'desc'    => __( 'Remove WooCommerce CSS & Scripts unless the user is on a WooCommerce page. Thanks to <a href="https://gist.github.com/gregrickaby/2846416" target="_blank">Greg Rickaby</a> for the function script.', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                ),
                /*
                array(
                    'name'    => 'lazyload_speedup',
                    'label'   => __( 'LazyLoad for Images', 'dwdtweaker' ),
                    'desc'    => __( 'This will speeds up your Divi site by loading images only as they enter the viewport. This will affect only img tag at the moment.', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                ),
                */
                array(
                    'name'    => 'query_arg_speedup',
                    'label'   => __( 'Remove Query Strings', 'dwdtweaker' ),
                    'desc'    => __( 'This will remove Query Strings from Static Resources that have either “?” or “&”. This is useful for those who use cache plugin as this ensures that they are cached like other elements. Even without cache plugin this will improve your speed scores in services like Pingdom, GTmetrix, PageSpeed and YSlow.', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'cdn_speedup',
                    'label'   => __( 'Replace Local file with CDN', 'dwdtweaker' ),
                    'desc'    => __( 'This will replace local jQuery with CDN jQuery link and also replace Divi fitvids, waypoints and magnific-popup scripts with CDN link. (Note: Divi now allows you to Minify And Combine Javascript Files as well. If you enabled that, this option is not needed)', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                ),
            ),
            'dwd_tweaker_page_transition' => array(
                array(
                    'name'    => 'dwd_page_transition_on_off',
                    'label'   => __( 'Enable Page Transition', 'dwdtweaker' ),
                    'desc'    => __( 'This will enable page transition on your Divi site.', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'dwd_page_in_animation',
                    'label'   => __( 'Page Transition In', 'dwdtweaker' ),
                    'desc'    => __( 'Select your transition for page in.', 'dwdtweaker' ),
                    'type'    => 'select',
                    'default' => 'fade-in',
                    'options' => array(
                        'fade-in' => 'Fade In',
                        'fade-in-down-sm'  => 'Fade In Down Small',
                        'fade-in-down-lg' => 'Fade In Down Large',
                        'fade-in-up-sm'  => 'Fade In Up Small',
                        'fade-in-up-lg' => 'Fade In Up Large',
                        'fade-in-left'  => 'Fade In Left',
                        'fade-in-left-sm' => 'Fade In Left Small',
                        'fade-in-left-lg' => 'Fade In Left Large',
                        'fade-in-right'  => 'Fade In Right',
                        'fade-in-right-sm' => 'Fade In Right Small',
                        'fade-in-right-lg' => 'Fade In Right Large',
                        'rotate-in'  => 'Rotate In',
                        'rotate-in-sm' => 'Rotate In Small',
                        'rotate-in-lg' => 'Rotate In Large',
                        'zoom-in'  => 'Zoom In',
                        'zoom-in-sm' => 'Zoom In Small',
                        'zoom-in-lg' => 'Zoom In Large',
                        'flip-in-x'  => 'Flip In X',
                        'flip-in-x-nr' => 'Flip In X Near',
                        'flip-in-x-fr' => 'Flip In X Far',
                        'flip-in-y'  => 'Flip In Y',
                        'flip-in-y-nr' => 'Flip In Y Near',
                        'flip-in-y-fr' => 'Flip In Y Far'
                    )
                ),
                array(
                    'name'    => 'dwd_page_out_animation',
                    'label'   => __( 'Page Transition Out', 'dwdtweaker' ),
                    'desc'    => __( 'Select your transition for page out.', 'dwdtweaker' ),
                    'type'    => 'select',
                    'default' => 'fade-out',
                    'options' => array(
                        'fade-out' => 'Fade Out',
                        'fade-out-down-sm'  => 'Fade Out Down Small',
                        'fade-out-down-lg'  => 'Fade Out Down Large',
                        'fade-out-up-sm'  => 'Fade Out Up Small',
                        'fade-out-up-lg'  => 'Fade Out Up Large',
                        'fade-out-left' => 'Fade Out Left',
                        'fade-out-left-sm'  => 'Fade Out Left Small',
                        'fade-out-left-lg'  => 'Fade Out Left Large',
                        'fade-out-right' => 'Fade Out Right',
                        'fade-out-right-sm'  => 'Fade Out Right Small',
                        'fade-out-right-lg'  => 'Fade Out Right Large',
                        'rotate-out' => 'Rotate Out',
                        'rotate-out-sm'  => 'Rotate Out Small',
                        'rotate-out-lg'  => 'Rotate Out Large',
                        'zoom-out' => 'Zoom Out',
                        'zoom-out-sm'  => 'Zoom Out Small',
                        'zoom-out-lg'  => 'Zoom Out Large',
                        'flip-out-x'  => 'Flip Out X',
                        'flip-out-x-nr' => 'Flip Out X Near',
                        'flip-out-x-fr' => 'Flip Out X Far',
                        'flip-out-y'  => 'Flip Out Y',
                        'flip-out-y-nr' => 'Flip Out Y Near',
                        'flip-out-y-fr' => 'Flip Out Y Far'
                    )
                ),
                array(
                    'name'              => 'dwd_page_in_duration',
                    'label'             => __( 'Page in Duration', 'dwdtweaker' ),
                    'desc'              => __( 'The page in animation duration.', 'dwdtweaker' ),
                    'placeholder'       => __( '1500', 'dwdtweaker' ),
                    'min'               => 500,
                    'max'               => 4000,
                    'step'              => '1',
                    'type'              => 'number',
                    'sanitize_callback' => 'floatval'
                ),
                array(
                    'name'              => 'dwd_page_out_duration',
                    'label'             => __( 'Page Out Duration', 'dwdtweaker' ),
                    'desc'              => __( 'The page out animation duration.', 'dwdtweaker' ),
                    'placeholder'       => __( '800', 'dwdtweaker' ),
                    'min'               => 100,
                    'max'               => 4000,
                    'step'              => '1',
                    'type'              => 'number',
                    'sanitize_callback' => 'floatval'
                ),
                array(
                    'name'    => 'dwd_page_loader',
                    'label'   => __( 'Show Loader', 'dwdtweaker' ),
                    'type'    => 'select',
                    'default' => 'true',
                    'options' => array(
                        'true' => 'Show',
                        'false'  => 'Dont Show'
                    )
                ),
                array(
                    'name'    => 'dwd_loader_color',
                    'label'   => __( 'Loader Color', 'dwdtweaker' ),
                    //'desc'    => __( 'Loader description', 'dwdtweaker' ),
                    'type'    => 'color',
                    'default' => '#eee'
                ),
                array(
                    'name'              => 'dwd_loader_border',
                    'label'             => __( 'Loader Border Size', 'dwdtweaker' ),
                    //'desc'              => __( 'The page out animation duration.', 'dwdtweaker' ),
                    'placeholder'       => __( '5', 'dwdtweaker' ),
                    'min'               => 1,
                    'max'               => 50,
                    'step'              => '1',
                    'type'              => 'number',
                    'sanitize_callback' => 'floatval',
                    'default' => '5'
                ),
                array(
                    'name'              => 'dwd_loader_size',
                    'label'             => __( 'Loader Size', 'dwdtweaker' ),
                    'desc'              => __( 'By default, the height and width is 32px. You can set your own size here.', 'dwdtweaker' ),
                    'placeholder'       => __( '32', 'dwdtweaker' ),
                    'min'               => 1,
                    'max'               => 200,
                    'step'              => '1',
                    'type'              => 'number',
                    'sanitize_callback' => 'floatval',
                    'default' => '32'
                ),
            ),
			'dwd_tweaker_design' => array(
                array(
                    'name'              => 'dwd_mobile_menu_text',
                    'label'             => __( 'Custom Mobile Text', 'dwdtweaker' ),
                    'desc'              => __( 'Leave blank if you dont wish to have custom mobile text. The text will appear next to the mobile menu icon. Depending on your text length, you might have to use custom CSS to adjust. However, we have already style up based on the text "Menu"', 'dwdtweaker' ),
                    'type'              => 'text',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'    => 'dwd_mobile_menu_text_color',
                    'label'   => __( 'Mobile Icon Text Color', 'dwdtweaker' ),
                    'type'    => 'color',
                    'default' => '#2ea3f2'
                ),
                array(
                    'name'    => 'dwd_open_mobile_menu_text_color',
                    'label'   => __( 'Mobile Fullscreen Icon Text Color', 'dwdtweaker' ),
                    'desc'              => __( 'This could be useful especially when using Fullscreen Header Style', 'dwdtweaker' ),
                    'type'    => 'color',
                    'default' => '#fff'
                ),
                array(
                    'name'    => 'dwd_design_theme_toolbar',
                    'label'   => __( 'Use Browser Toolbar', 'dwdtweaker' ),
                    'desc'    => __( 'This will allow you to set the color of the browser toolbar or as known as Browser Tab color. Compatibile with Android, iOS and Windows Phone and based on Google guidelines.', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'dwd_design_theme_color',
                    'label'   => __( 'Browser Toolbar Color', 'dwdtweaker' ),
                    'type'    => 'color',
                ),
                array(
                    'name'    => 'dwd_design_material',
                    'label'   => __( 'Ripple Effect on Buttons & Tabs', 'dwdtweaker' ),
                    'desc'    => __( 'This will have material ripple effect on Divi buttons and Tabs. Powered by <a href="https://github.com/virtyaluk/paper-ripple" target="_blank">paperRipple</a>', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'dwd_button_hover_animation',
                    'label'   => __( 'Button Hover Animation', 'dwdtweaker' ),
                    'desc'    => __( 'Select a hover animation for your Divi buttons. This will apply animation to classes that has .et_pb_button', 'dwdtweaker' ),
                    'type'    => 'select',
                    'default' => 'none',
                    'options' => array(
                        '' => 'None',
                        'hvr-grow' => 'Grow',
                        'hvr-shrink' => 'Shrink',
                        'hvr-pulse' => 'Pulse',
                        'hvr-pulse-grow' => 'Pulse Grow',
                        'hvr-pulse-shrink' => 'Pulse Shrink',
                        'hvr-push' => 'Push',
                        'hvr-pop' => 'Pop',
                        'hvr-bounce-in' => 'Bounce In',
                        'hvr-bounce-out' => 'Bounce Out',
                        'hvr-rotate' => 'Rotate',
                        'hvr-grow-rotate' => 'Grow Rotate',
                        'hvr-float' => 'Float',
                        'hvr-sink' => 'Sink',
                        'hvr-bob' => 'Bob',
                        'hvr-hang' => 'Hang',
                        'hvr-skew' => 'Skew',
                        'hvr-skew-forward' => 'Skew Forward',
                        'hvr-skew-backward' => 'Skew Backward',
                        'hvr-wobble-vertical' => 'Wobble Vertical',
                        'hvr-wobble-horizontal' => 'Wobble Horizontal',
                        'hvr-wobble-to-bottom-right' => 'Wobble To Bottom Right',
                        'hvr-wobble-to-top-right' => 'Wobble To Top Right',
                        'hvr-wobble-top' => 'Wobble Top',
                        'hvr-wobble-bottom' => 'Wobble Bottom',
                        'hvr-wobble-skew' => 'Wobble Skew',
                        'hvr-buzz' => 'Buzz',
                        'hvr-buzz-out' => 'Buzz Out'
                    )
                ),
                array(
                    'name'    => 'dwd_design_pull_to_refresh',
                    'label'   => __( 'Pull to Refresh (PTR)', 'dwdtweaker' ),
                    'desc'    => __( 'This will enable pull-to-refresh (or swipe-to-refresh) pattern lets a user pull down on a list of data using touch in order to retrieve more data or refreshing the page. Powered by <a href="https://github.com/BoxFactura/pulltorefresh.js" target="_blank">PulltoRefresh.js</a>', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'              => 'dwd_design_pull_to_refresh_text',
                    'label'             => __( '(PTR) Pull to Refresh Text', 'dwdtweaker' ),
                    'desc'              => __( 'The initial text when user starts to Pull.', 'dwdtweaker' ),
                    'placeholder'       => __( 'Pull down to refresh', 'dwdtweaker' ),
                    'type'              => 'text',
                    'default'           => 'Pull down to refresh',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'dwd_design_release_to_refresh_text',
                    'label'             => __( '(PTR) Pull to Refresh Text', 'dwdtweaker' ),
                    'desc'              => __( 'The instructions text when the pull to refresh height has been reached.', 'dwdtweaker' ),
                    'placeholder'       => __( 'Release to refresh', 'dwdtweaker' ),
                    'type'              => 'text',
                    'default'           => 'Release to refresh',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'dwd_design_ptr_refreshing_text',
                    'label'             => __( '(PTR) Refreshing Text', 'dwdtweaker' ),
                    'desc'              => __( 'The refreshing text when page is loading.', 'dwdtweaker' ),
                    'placeholder'       => __( 'Refreshing', 'dwdtweaker' ),
                    'type'              => 'text',
                    'default'           => 'Refreshing',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'    => 'dwd_design_ptr_icon_color',
                    'label'   => __( '(PTR) Icon Color', 'dwdtweaker' ),
                    'type'    => 'color',
                    'default' => '#a3a3a3'
                ),
                array(
                    'name'    => 'dwd_design_ptr_text_color',
                    'label'   => __( '(PTR) Text Color', 'dwdtweaker' ),
                    'type'    => 'color',
                    'default' => '#a3a3a3'
                ),
                array(
                    'name'    => 'dwd_design_ptr_bg_color',
                    'label'   => __( '(PTR) Background Color', 'dwdtweaker' ),
                    'type'    => 'color',
                    'default' => '#fff'
                )
            ),
            'dwd_tweaker_advanced' => array(
                array(
                    'name'              => 'dwd_custom_divi_builder_label',
                    'label'             => __( 'Custom Divi Builder Label', 'dwdtweaker' ),
                    'desc'              => __( 'Leave blank if you don&apos;t wish to have custom Divi Builder Label. This will change the label on the Divi Builder, Admin Menu, Divi Theme Option and also remove the icon before the label text. This has been tested with Divi and Extra Theme.', 'dwdtweaker' ),
                    'type'              => 'text',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'    => 'dwd_clear_local_storage',
                    'label'   => __( 'Display Clear Divi Local Storage Button', 'dwdtweaker' ),
                    'desc'    => __( 'This show a button on the Divi Page Builder area to clear the localStorage on your browser. Use this when you are experiencing Quota Exceeded for copy and paste module. This is very useful when Divi or 3rd party modules get updated.', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'dwd_fullscreen_modal',
                    'label'   => __( 'Fullscreen Divi Builder Modal', 'dwdtweaker' ),
                    'desc'    => __( 'This will make the Divi Page Builder Modal expanded to fullscreen size. The text in the Divi Builder will be scaled down to a smaller size as well but readable on all screen sizes. Very useful when using custom third-party Divi modules which has alot of option.', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'dwd_divi_custom_post',
                    'label'   => __( 'Enable Divi Builder on Custom Post Types', 'dwdtweaker' ),
                    'desc'    => __( 'This will add Divi Builder and Visual Builder to Custom Post Types.', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'dwd_divi_show_image_input',
                    'label'   => __( 'Show Image URL input field on the New Divi Background UI', 'dwdtweaker' ),
                    'desc'    => __( 'This will show the hidden image input field on the new background UI and allow you to have custom URL link which Elegant Themes removed. This will/should work with the default Divi modules or third-party Divi modules.', 'dwdtweaker' ),
                    'type'  => 'checkbox'
                )
            )
        );

        return $settings_fields;
    }

    function plugin_page() { ?>
        <div class="wrap">
        <?php
	        if ( is_plugin_inactive('woocommerce/woocommerce.php') && my_get_option( 'woocommerce_speedup', 'dwd_tweaker_speed') === 'on' ) {
	            echo '<div class="notice notice-warning is-dismissible"> 
	                    <p><strong>You do not have WooCommerce Install.</strong> You must need to install "WooCommerce" plugin first in order to use Speed Up WooCommerce Tweak.</p>
	                    <button type="button" class="notice-dismiss">
	                        <span class="screen-reader-text">Dismiss this notice.</span>
	                    </button>
	                </div>';
	        }
	        $this->settings_api->show_navigation();
	        $this->settings_api->show_forms();
	        ?>
	        <div style="margin:auto"><a href="https://www.elegantthemes.com/affiliates/idevaffiliate.php?id=34051_1_1_13" target="_blank" rel="nofollow"><img style="border:0px" src="https://www.elegantthemes.com/affiliates/media/banners/570x100.jpg" width="570" height="100" alt=""></a></div>
	        <p>This is a free WordPress plugin that will help over 300,000 Divi users to enhance their Divi Experiences. If this is helpful for you, buy me a Coffee via <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=sales@diviwebdesign.com&item_name=Divi+Web+Design&item_number=Divi+Tweaker&currency_code=USD" target="_blank">PayPal</a><a style="text-align:right; float:right;" href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=sales@diviwebdesign.com&item_name=Divi+Web+Design&item_number=Divi+Tweaker&currency_code=USD" target="_blank"><img src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_donate_92x26.png"
    alt="Donate"></a></p>
	        <hr>
	        <a href="https://diviwebdesign.com">Divi Tweaker</a> v<?php echo plugin_get_version(); ?> You can report any bugs <a href="https://diviwebdesign.com">here</a>
	        </div>
        <?php
    }

    /**
     * Get all the pages
     *
     * @return array page names with key value pairs
     */
    function get_pages() {
        $pages = get_pages();
        $pages_options = array();
        if ( $pages ) {
            foreach ($pages as $page) {
                $pages_options[$page->ID] = $page->post_title;
            }
        }

        return $pages_options;
    }

}
endif;