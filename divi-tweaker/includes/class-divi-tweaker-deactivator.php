<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://diviwebdesign.com/
 * @since      1.0.0
 *
 * @package    Divi_Tweaker
 * @subpackage Divi_Tweaker/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Divi_Tweaker
 * @subpackage Divi_Tweaker/includes
 * @author     Divi Web Design <hello@diviwebdesign.com>
 */
class Divi_Tweaker_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
