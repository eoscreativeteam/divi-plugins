<?php

/**
 * Fired during plugin activation
 *
 * @link       https://diviwebdesign.com/
 * @since      1.0.0
 *
 * @package    Divi_Tweaker
 * @subpackage Divi_Tweaker/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Divi_Tweaker
 * @subpackage Divi_Tweaker/includes
 * @author     Divi Web Design <hello@diviwebdesign.com>
 */
class Divi_Tweaker_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
