<?php

/**
 *
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://diviwebdesign.com/
 * @since             1.1.2
 * @package           Divi_Tweaker
 *
 * @wordpress-plugin
 * Plugin Name:       Divi Tweaker
 * Plugin URI:        https://diviwebdesign.com/
 * Description:       A Divi Tweak Tool that make your Divi fly high without coding. Speed optimizing, page transitions and more. This is currently in Beta
 * Version:           1.1.2
 * Author:            Divi Web Design
 * Author URI:        https://diviwebdesign.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       divi-tweaker
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-divi-tweaker-activator.php
 */
function activate_divi_tweaker() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-divi-tweaker-activator.php';
	Divi_Tweaker_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-divi-tweaker-deactivator.php
 */
function deactivate_divi_tweaker() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-divi-tweaker-deactivator.php';
	Divi_Tweaker_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_divi_tweaker' );
register_deactivation_hook( __FILE__, 'deactivate_divi_tweaker' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-divi-tweaker.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/class-divi-settings-api.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/divi-tweaker-page-option.php';
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.1.1
*/

function run_divi_tweaker() {

	$plugin = new Divi_Tweaker();
	$plugin->run();
}
run_divi_tweaker();

new DWD_Tweaker_Settings_Page();

function my_get_option( $option, $section, $default = '' ) {
 
    $options = get_option( $section );
 
    if ( isset( $options[$option] ) ) {
    return $options[$option];
    }
 
    return $default;
}

function plugin_get_version() {
	if ( ! function_exists( 'get_plugins' ) )
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	$plugin_folder = get_plugins( '/' . plugin_basename( dirname( __FILE__ ) ) );
	$plugin_file = basename( ( __FILE__ ) );
	return $plugin_folder[$plugin_file]['Version'];
}

/**Divi**/
function deregister_divi_script() {
	$theme = wp_get_theme(); // gets the current theme
	if ( 'Divi' == $theme->name || 'Divi' == $theme->parent_theme || 'Extra' == $theme->name || 'Extra' == $theme->parent_theme ) {
	    $is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
	    if( !$is_page_builder_used ) {
        //wp_dequeue_script('et-builder-modules-global-functions-script');
        wp_dequeue_script('google-maps-api');
        wp_dequeue_script('divi-fitvids');
        wp_dequeue_script('waypoints');
        wp_dequeue_script('magnific-popup');
          
        wp_dequeue_script('hashchange');
        wp_dequeue_script('salvattore');
        wp_dequeue_script('easypiechart');
          
        wp_dequeue_script('et-jquery-visible-viewport');
          
        wp_dequeue_script('magnific-popup');
        wp_dequeue_script('et-jquery-touch-mobile');
        wp_dequeue_script('et-builder-modules-script');
    }
	}
}

function deregister_divi_styles() {
	$theme = wp_get_theme(); // gets the current theme
	if ( 'Divi' == $theme->name || 'Divi' == $theme->parent_theme || 'Extra' == $theme->name || 'Extra' == $theme->parent_theme ) {
	    $is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
	  
	    if( !$is_page_builder_used ) {
	        wp_dequeue_style('et-builder-modules-style');
	    }
	}
}

if (my_get_option( 'divi_speedup', 'dwd_tweaker_speed') === 'on' ) {
	add_action( 'wp_print_styles', 'deregister_divi_styles', 100 );
	add_action( 'wp_print_scripts', 'deregister_divi_script', 100 );
}

/*WooCommerce*/
function dwd_tweak_manage_woocommerce_styles() {
    // Remove the generator tag
	remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );
	// Unless we're in the store, remove all the cruft!
	if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
		wp_dequeue_style( 'woocommerce_frontend_styles' );
		wp_dequeue_style( 'woocommerce_fancybox_styles' );
		wp_dequeue_style( 'woocommerce_chosen_styles' );
		wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
		wp_dequeue_style( 'select2' );
		wp_dequeue_script( 'wc-add-payment-method' );
		wp_dequeue_script( 'wc-lost-password' );
		wp_dequeue_script( 'wc_price_slider' );
		wp_dequeue_script( 'wc-single-product' );
		wp_dequeue_script( 'wc-add-to-cart' );
		wp_dequeue_script( 'wc-cart-fragments' );
		wp_dequeue_script( 'wc-credit-card-form' );
		wp_dequeue_script( 'wc-checkout' );
		wp_dequeue_script( 'wc-add-to-cart-variation' );
		wp_dequeue_script( 'wc-single-product' );
		wp_dequeue_script( 'wc-cart' );
		wp_dequeue_script( 'wc-chosen' );
		wp_dequeue_script( 'woocommerce' );
		wp_dequeue_script( 'prettyPhoto' );
		wp_dequeue_script( 'prettyPhoto-init' );
		wp_dequeue_script( 'jquery-blockui' );
		wp_dequeue_script( 'jquery-placeholder' );
		wp_dequeue_script( 'jquery-payment' );
		wp_dequeue_script( 'fancybox' );
		wp_dequeue_script( 'jqueryui' );
	}
}

if (my_get_option( 'woocommerce_speedup', 'dwd_tweaker_speed') === 'on' ) {
	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		add_action( 'wp_enqueue_scripts', 'dwd_tweak_manage_woocommerce_styles', 99 );
	}
}

/* Page Transition */
function divi_page_transition_load() {
	wp_enqueue_style('animsition-style', plugin_dir_url( __FILE__ ) . 'public/css/animsition.min.css' );
	$loadercolor = my_get_option( 'dwd_loader_color', 'dwd_tweaker_page_transition' );
	$loaderwidth = my_get_option( 'dwd_loader_border', 'dwd_tweaker_page_transition' );
	$loadersize = my_get_option( 'dwd_loader_size', 'dwd_tweaker_page_transition' );
	$loaderwidthsmall = ($loadersize) / 2;
	$dwd_page_transition_style = ".animsition-loading { border-left-color: {$loadercolor}; border-width: {$loaderwidth}px; } .animsition-loading, .animsition-loading:after { height: {$loadersize}px; width: {$loadersize}px; margin-top: -{$loaderwidthsmall}px; margin-left: -{$loaderwidthsmall}px; }";
    wp_add_inline_style( 'animsition-style', $dwd_page_transition_style );
	wp_enqueue_script('animsition', plugin_dir_url( __FILE__ ) . 'public/js/animsition.min.js', array('jquery'), '4.0.2', true );
	wp_enqueue_script('animsition-start', plugin_dir_url( __FILE__ ) . 'public/js/animsition.js', array('animsition'), '1.0', true );
	$transitionData = array( 'inclass' => my_get_option( 'dwd_page_in_animation', 'dwd_tweaker_page_transition' ), 'outclass' => my_get_option( 'dwd_page_out_animation', 'dwd_tweaker_page_transition' ), 'induration' => my_get_option( 'dwd_page_in_duration', 'dwd_tweaker_page_transition' ), 'outduration' => my_get_option( 'dwd_page_out_duration', 'dwd_tweaker_page_transition' ), 'loader' => my_get_option( 'dwd_page_loader', 'dwd_tweaker_page_transition' )  );
	wp_localize_script( 'animsition-start', 'dwd_page_transition_option_var', $transitionData );
}

function dwd_transition_body_class( $classes ) {
    $classes[] = 'animsition';
    return $classes;
}

//dwd_loader_color
if (my_get_option( 'dwd_page_transition_on_off', 'dwd_tweaker_page_transition' ) === 'on' ) {
	add_filter( 'body_class','dwd_transition_body_class' );
	add_action ('wp_enqueue_scripts', 'divi_page_transition_load');
}
/*Remove Query String*/
function dwd_remove_script_version( $src ){
	$parts = explode( '?ver', $src );
	return $parts[0];
}
if (my_get_option( 'query_arg_speedup', 'dwd_tweaker_speed' ) === 'on' ) {
	add_filter( 'script_loader_src', 'dwd_remove_script_version', 15, 1 );
	add_filter( 'style_loader_src', 'dwd_remove_script_version', 15, 1 );
}

/*Jquery CDN*/
function dwd_replace_CDN() {
  //set protocol dynamically
    if( is_ssl() ){
        $protocol = 'https';
    }else {
        $protocol = 'http';
    }
  	global $wp_scripts;
  	$wp_scripts->registered['jquery-core']->src = $protocol . '://ajax.googleapis.com/ajax/libs/jquery/' . $wp_scripts->registered['jquery-core']->ver . '/jquery.min.js';
  	/*
  	wp_enqueue_script( 'jquery' );
	$wp_jquery_ver = $GLOBALS['wp_scripts']->registered['jquery']->ver; 
	$jquery_ver = $wp_jquery_ver == '' ? '1.11.4' : $wp_jquery_ver;

	wp_deregister_script( 'jquery-core' );
	wp_register_script( 'jquery-core', $protocol . '://ajax.googleapis.com/ajax/libs/jquery/'. $jquery_ver .'/jquery.min.js' );
	wp_enqueue_script( 'jquery' );
	*/
}

function dwd_replace_divi_cdn() {
	if( is_ssl() ){
        $protocol = 'https';
    }else {
        $protocol = 'http';
    }
    wp_dequeue_script( 'divi-fitvids' );
    wp_enqueue_script('divi-fitvids', $protocol . '://cdnjs.cloudflare.com/ajax/libs/fitvids/1.1.0/jquery.fitvids.min.js', array('jquery'), '1.0.0', true );
  	wp_dequeue_script( 'waypoints' );
    wp_enqueue_script('waypoints', $protocol . '://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.min.js', array('jquery'), '4.0.0', true );
  	wp_dequeue_script( 'magnific-popup' );
    wp_enqueue_script( 'magnific-popup', $protocol . '://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.1/jquery.magnific-popup.min.js', array('jquery'), '1.0.0', true );
}

if (my_get_option( 'cdn_speedup', 'dwd_tweaker_speed' ) === 'on' ) {
	add_action( 'wp_enqueue_scripts', 'dwd_replace_CDN' );
	add_action( 'wp_enqueue_scripts', 'dwd_replace_divi_cdn', 11 );
}

/*Design Related*/
function divi_material_design() {
	wp_enqueue_style('paperRipple-style', plugin_dir_url( __FILE__ ) . 'public/css/paper-ripple.min.css' );
	wp_enqueue_script('paperRipple', plugin_dir_url( __FILE__ ) . 'public/js/paperRipple.jquery.min.js', array('jquery'), '0.3.0', true );
	wp_enqueue_script('paperRipple-start', plugin_dir_url( __FILE__ ) . 'public/js/paperRipple-start.js', array('paperRipple'), '1.0', true );
}
if (my_get_option( 'dwd_design_material', 'dwd_tweaker_design' ) === 'on' ) {
	add_action ( 'wp_enqueue_scripts', 'divi_material_design' );
}
/*Hover Animation*/
function divi_button_hover_animation() {
	wp_enqueue_style('hover-style', plugin_dir_url( __FILE__ ) . 'public/css/hover-min.css' );
	wp_enqueue_script('divi-button-hover-start', plugin_dir_url( __FILE__ ) . 'public/js/divi-button-hover-start.js', array('jquery'), '1.0', true );
	$hoverData = array( 'buttonhover' => my_get_option( 'dwd_button_hover_animation', 'dwd_tweaker_design' )  );
	wp_localize_script( 'divi-button-hover-start', 'dwd_button_hover_animation_var', $hoverData );
}
if (my_get_option( 'dwd_button_hover_animation', 'dwd_tweaker_design' ) !== 'none' ) {
	add_action ( 'wp_enqueue_scripts', 'divi_button_hover_animation' );
}
/*Clear Divi Local Storage*/
function dwd_clear_local_storage () {
	wp_enqueue_script( 'dwd_clear_local_storage', plugin_dir_url( __FILE__ ) . 'admin/js/dwd-clearlocal.js' );
}

if (my_get_option( 'dwd_clear_local_storage', 'dwd_tweaker_advanced' ) === 'on' ) {
	add_action( 'admin_enqueue_scripts', 'dwd_clear_local_storage', 9999 );
}

/*Pull to refresh*/
function divi_pulltorefresh() {
	wp_enqueue_style('dwd-ptr-style', plugin_dir_url( __FILE__ ) . 'public/css/dwd-ptr-style.css' );
	$iconcolor = my_get_option( 'dwd_design_ptr_icon_color', 'dwd_tweaker_design' );
	$textcolor = my_get_option( 'dwd_design_ptr_text_color', 'dwd_tweaker_design' );
	$bgcolor = my_get_option( 'dwd_design_ptr_bg_color', 'dwd_tweaker_design' );
	$dwd_ptr_style = ".dwd-ptr--icon { color: {$iconcolor} !important; } .dwd-ptr--text { color: {$textcolor} !important; } .dwd-ptr--box { background-color: {$bgcolor}; }";
    wp_add_inline_style( 'dwd-ptr-style', $dwd_ptr_style );
	wp_enqueue_script('pulltorefresh', plugin_dir_url( __FILE__ ) . 'public/js/pulltorefresh.js', array(), '1.0', true );
	wp_enqueue_script('pulltorefresh-start', plugin_dir_url( __FILE__ ) . 'public/js/pulltorefresh-start.js', array('pulltorefresh'), '1.0.0', true );
	$ptrData = array( 'ptrtext' => my_get_option( 'dwd_design_pull_to_refresh_text', 'dwd_tweaker_design' ), 'ptreleasetext' => my_get_option( 'dwd_design_release_to_refresh_text', 'dwd_tweaker_design' ), 'ptrefreshingtext' => my_get_option( 'dwd_design_ptr_refreshing_text', 'dwd_tweaker_design' ) );
	wp_localize_script( 'pulltorefresh-start', 'dwd_ptr_option_var', $ptrData );
}
if (my_get_option( 'dwd_design_pull_to_refresh', 'dwd_tweaker_design' ) === 'on' ) {
	add_action ( 'wp_enqueue_scripts', 'divi_pulltorefresh' );
}

/*Mobile text*/
function divi_mobile_text() {
	wp_enqueue_style('dwd-mobile-text-style', plugin_dir_url( __FILE__ ) . 'public/css/dwd-mobile-text.css' );
	$mobiletext = my_get_option( 'dwd_mobile_menu_text', 'dwd_tweaker_design' );
	$mobiletextcolor = my_get_option( 'dwd_mobile_menu_text_color', 'dwd_tweaker_design' );
	$mobileopentextcolor = my_get_option( 'dwd_open_mobile_menu_text_color', 'dwd_tweaker_design' );
	$dwd_mobile_text_style = ".mobile_menu_bar .dwd-mobile-text::after { content: '{$mobiletext}'; color: {$mobiletextcolor}; } .et_pb_fullscreen_menu_active .mobile_menu_bar.et_toggle_fullscreen_menu .dwd-mobile-text::after { color: {$mobileopentextcolor}; }";
	wp_add_inline_style( 'dwd-mobile-text-style', $dwd_mobile_text_style );
	wp_enqueue_script('dwd-mobile-text', plugin_dir_url( __FILE__ ) . 'public/js/dwd-mobile-text.js', array(), '1.0', true );
}

if ( '' !== my_get_option( 'dwd_mobile_menu_text', 'dwd_tweaker_design' ) ) {
	add_action ( 'wp_enqueue_scripts', 'divi_mobile_text', 11 );
}

/*Theme Color Browser Toolbar*/
function divi_design_theme_toolbar() { ?>
<!-- Chrome, Firefox OS and Opera -->
<meta name="theme-color" content="<?php echo my_get_option( 'dwd_design_theme_color', 'dwd_tweaker_design' ); ?>"/>
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="<?php echo my_get_option( 'dwd_design_theme_color', 'dwd_tweaker_design' ); ?>"/>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<?php
}

if (my_get_option( 'dwd_design_theme_toolbar', 'dwd_tweaker_design' ) === 'on' ) {
	add_action ( 'wp_head', 'divi_design_theme_toolbar', 5 );
}

/*Enable Divi Builder on Custom Post 
Use DIVI Page-Builder templates Globally - ALWAYS. With this, all post types use the same pool of section templates */
function dwd_custom_post_types($post_types)
{
    foreach (get_post_types() as $post_type) {
        if (!in_array($post_type, $post_types) and post_type_supports($post_type, 'editor')) {
            $post_types[] = $post_type;
        }
    }
    return $post_types;
}

function dwd_custom_add_meta_box()
{
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'editor') and function_exists('et_single_settings_meta_box')) {
            $obj= get_post_type_object( $post_type );
            add_meta_box('et_settings_meta_box', sprintf(__('Divi %s Settings', 'Divi'), $obj->labels->singular_name), 'et_single_settings_meta_box', $post_type, 'side', 'high');
        }
        //add_meta_box( 'et_pb_pagebuilder_meta_box', esc_html__( 'asdasd', 'et_builder' ), 'et_pb_pagebuilder_meta_box', $post_type, 'normal', 'high' );
    }
}

function dwd_enable_builder_admin_js()
{
    $s = get_current_screen();
    if (!empty($s->post_type) and $s->post_type != 'page' and $s->post_type != 'post') {
        ?>
        <script>
            jQuery(function ($) {
                $('#et_pb_layout').insertAfter($('#et_pb_main_editor_wrap'));
            });
        </script>
        <?php
    }
}

function dwd_divi_overrule_post_types() {
	return get_post_types();
}

if (my_get_option( 'dwd_divi_custom_post', 'dwd_tweaker_advanced' ) === 'on' ) {
	add_filter('et_builder_post_types', 'dwd_divi_overrule_post_types', 100);
	add_filter('et_fb_post_types','dwd_divi_overrule_post_types' );
	add_action('add_meta_boxes', 'dwd_custom_add_meta_box');
	add_action('admin_head', 'dwd_enable_builder_admin_js');
	add_filter('et_pb_show_all_layouts_built_for_post_type', 'dwd_divi_overrule_post_types', 100);
}


/*Custom Label*/
function dwd_custom_divi_label(){
	add_action( 'add_meta_boxes', 'dwd_add_custom_divi_label' );
}

function dwd_add_custom_divi_label() {
	$post_types = et_builder_get_builder_post_types();
	$theme = wp_get_theme(); // gets the current theme
	if ( 'Divi' == $theme->name || 'Divi' == $theme->parent_theme || 'Extra' == $theme->name || 'Extra' == $theme->parent_theme ) {
	    $is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
	    $dwd_divi_label = my_get_option( 'dwd_custom_divi_builder_label', 'dwd_tweaker_advanced');
		foreach ( $post_types as $post_type ){
			add_meta_box( ET_BUILDER_LAYOUT_POST_TYPE, esc_html__( $dwd_divi_label, 'et_builder' ), 'et_pb_pagebuilder_meta_box', $post_type, 'normal', 'high' );
		}
	}
}

function dwd_remove_divi_label_icon() {
	echo '<style>
  	#et_pb_layout .hndle {
  		padding-left: 40px !important;
	}
    #et_pb_layout .hndle:before, .toplevel_page_et_extra_options h1#epanel-title:before, .toplevel_page_et_divi_options h1#epanel-title:before {
    	display: none;
    }
    .toplevel_page_et_extra_options h1#epanel-title, .toplevel_page_et_divi_options h1#epanel-title {
    	padding-left: 0;
    }

    li.toplevel_page_et_divi_options div.wp-menu-image:before, li.toplevel_page_et_extra_options div.wp-menu-image:before {
    	font-family: dashicons;
    	content: "\f109";
    	width: 20px !important;
    	font-size: 20px !important;
    	margin-top: 0;
    }
  	</style>
  	';
}

function dwd_remove_divi_label_script() {
	wp_enqueue_script('dwd-white-label', plugin_dir_url( __FILE__ ) . 'admin/js/dwd-divi-white-label.js', array('jquery'), '1.0' );
	$dwdpasslabel = array( 'dwd_custom_divi_label_text' => my_get_option( 'dwd_custom_divi_builder_label', 'dwd_tweaker_advanced' ) );
	wp_localize_script( 'dwd-white-label', 'dwd_custom_label_var', $dwdpasslabel );
}

if ( '' !== my_get_option( 'dwd_custom_divi_builder_label', 'dwd_tweaker_advanced') ) {
	add_action( 'add_meta_boxes', 'dwd_custom_divi_label', 5 );
	add_action( 'admin_head', 'dwd_remove_divi_label_icon' );
	add_action( 'admin_enqueue_scripts', 'dwd_remove_divi_label_script', 999 );
	add_action( 'admin_menu', 'dwd_white_label_admin_menu', 999 );
	add_filter( 'gettext', 'my_text_strings', 10, 3 );
}

/*https://gist.github.com/pawpawproject/a2399951104bf25f2233e230b41b93ed/*/


function dwd_white_label_admin_menu() 
{
    global $menu;

    // Pinpoint menu item
    $dwddivitheme = recursive_array_search( 'Divi', $menu );
    $dwdextratheme = recursive_array_search( 'Divi', $menu );
    $dwd_divi_label = my_get_option( 'dwd_custom_divi_builder_label', 'dwd_tweaker_advanced');
    // Validate
    if( !$dwddivitheme )
        return;
    if( !$dwdextratheme )
        return;

    $menu[$dwddivitheme][0] = $dwd_divi_label;
    $menu[$dwdextratheme][0] = $dwd_divi_label;
}
function recursive_array_search( $needle, $haystack ) 
{
    foreach( $haystack as $key => $value ) 
    {
        $current_key = $key;
        if( 
            $needle === $value 
            OR ( 
                is_array( $value )
                && recursive_array_search( $needle, $value ) !== false 
            )
        ) 
        {
            return $current_key;
        }
    }
    return false;
}

function my_text_strings( $translated_text, $text, $domain ) {
	$dwd_divi_label = my_get_option( 'dwd_custom_divi_builder_label', 'dwd_tweaker_advanced');
	switch ( $translated_text ) {
		case 'Divi Library' :
			$translated_text = __( $dwd_divi_label . ' Library', 'et_build_epanel' );
			break;
		case 'Divi' :
			$translated_text = __( $dwd_divi_label, 'et_build_epanel' );
			break;
		case 'Extra' :
			$translated_text = __( $dwd_divi_label, 'et_build_epanel' );
			break;
	}
	return $translated_text;
}

/* Show Image URL on new Background UI */
function divi_show_image_url() {
	wp_enqueue_style('dwd-fix-image-url', plugin_dir_url( __FILE__ ) . 'admin/css/dwd-fix-image-url.css' );
}

if ( my_get_option( 'dwd_divi_show_image_input', 'dwd_tweaker_advanced') === 'on' ) {
	add_action( 'admin_enqueue_scripts', 'divi_show_image_url', 11 );
}

/* Fullscreen Divi modal */
function dwd_fullscreen_modal_mode() {
	wp_enqueue_style('dwd-builder-fullscreen', plugin_dir_url( __FILE__ ) . 'admin/css/dwd-builder-fullscreen.css' );
}

if ( my_get_option( 'dwd_fullscreen_modal', 'dwd_tweaker_advanced') === 'on' ) {
	add_action( 'admin_enqueue_scripts', 'dwd_fullscreen_modal_mode', 11 );
}

/*Interactive BG 
function divi_interative_bg() {
	wp_enqueue_script('dwd-interactive-bg', plugin_dir_url( __FILE__ ) . 'public/js/interactive-background.js', array('jquery'), '1.0', true );
}
add_action ('wp_enqueue_scripts', 'divi_interative_bg');*/

/*
function dwd_customize_register($wp_customize) {
	$wp_customize->add_panel( 'dwd_tweaker' , array(
		'title'		=> esc_html__( 'Divi Tweaker', 'Divi' ),
		'priority'	=> 20,
	) );
    $wp_customize->add_section( 'et_divi_footer_credits' , array(
        'title'		=> esc_html__( 'Footer Credits', 'Divi' ),
        'panel' => 'dwd_tweaker',
    ) );
    $wp_customize->add_setting('et_divi[footer_credit_company]', array(
        'default' => 'Company Name',
        'type' => 'option',
        'capability' => 'edit_theme_options',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control('et_divi[footer_credit_company]', array(
        'label' => __('Enter Company Name', 'Divi'),
        'section' => 'et_divi_footer_credits',
        'type' => 'text',
    ));
    $wp_customize->add_setting('et_divi[footer_credit_url]', array(
        'default' => 'http://www.example.com/',
        'type' => 'option',
        'capability' => 'edit_theme_options',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control('et_divi[footer_credit_url]', array(
        'label' => __('Enter Company URL', 'Divi'),
        'section' => 'et_divi_footer_credits',
        'type' => 'text',
    ));
}
add_action('customize_register', 'dwd_customize_register', 11);
*/