/* global PullToRefresh */
(function($) {
    'use strict';
    $(function() {
        var getPageContainerTop = parseInt($('#page-container').css('padding-top'), 10);
        if ( $('.dwd-ptr--ptr').length > 0 ) {
			$('.dwd-ptr--box').css('margin-top', getPageContainerTop).css('position','absolute').css('width','100%');
		};
    });
})(jQuery);
PullToRefresh.init({
    mainElement: 'body',
    classPrefix: 'dwd-ptr--',
    distReload: '60',
    instructionsPullToRefresh: dwd_ptr_option_var.ptrtext,
    instructionsReleaseToRefresh: dwd_ptr_option_var.ptreleasetext,
    instructionsRefreshing: dwd_ptr_option_var.ptrefreshingtext,
    onRefresh: function(){ window.location.reload(); }
});
