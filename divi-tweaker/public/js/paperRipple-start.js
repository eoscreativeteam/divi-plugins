(function($) {
    'use strict';
    $(function() {
        $('input#searchsubmit, .et_pb_button, .et_pb_tabs .et_pb_tabs_controls a').paperRipple();
    });
})(jQuery);
