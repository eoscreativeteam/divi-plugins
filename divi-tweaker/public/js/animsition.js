(function($) {
    'use strict';
    $(document).ready(function() {
        $('a:not([target="_blank"]):not([href^="#"]):not([href^="mailto:"]):not(".et_pb_lightbox_image"):not(".download-button"):not(".et_pb_gallery_image a")' ).addClass('animsition-link');
        $(".animsition").animsition({
            inClass: dwd_page_transition_option_var.inclass,
            outClass: dwd_page_transition_option_var.outclass,
            inDuration: dwd_page_transition_option_var.induration,
            outDuration: dwd_page_transition_option_var.outduration,
            linkElement: '.animsition-link',
            // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
            loading: dwd_page_transition_option_var.loader,
            loadingParentElement: 'html', //animsition wrapper element
            loadingClass: 'animsition-loading',
            loadingInner: '', // e.g '<img src="loading.svg" />'
            timeout: false,
            timeoutCountdown: 5000,
            onLoadEvent: true,
            browser: ['animation-duration', '-webkit-animation-duration', '-o-animation-duration'],
            // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
            // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
            overlay: false,
            overlayClass: 'animsition-overlay-slide',
            overlayParentElement: 'body',
            transition: function(url) { window.location.href = url; }
        });
    });
})(jQuery);
