(function( $ ) {
	'use strict';
	jQuery(document).ready(function ($) {
		if ($('.toplevel_page_et_divi_options').length) {
		    $("#toplevel_page_et_divi_options .wp-menu-name").html($("#toplevel_page_et_divi_options .wp-menu-name").html().replace("Divi",dwd_custom_label_var.dwd_custom_divi_label_text));
		}
		if ($('.toplevel_page_et_divi_options #epanel-top').length) {
  			$(".toplevel_page_et_extra_options h1#epanel-title").html($(".toplevel_page_et_extra_options h1#epanel-title").html().replace("Divi Theme Options",dwd_custom_label_var.dwd_custom_divi_label_text + ' Theme Options'));
		}
  		if ($('.toplevel_page_et_extra_options').length) {
		    $("#toplevel_page_et_extra_options .wp-menu-name").html($("#toplevel_page_et_extra_options .wp-menu-name").html().replace("Extra",dwd_custom_label_var.dwd_custom_divi_label_text));
		}
		if ($('.toplevel_page_et_extra_options #epanel-top').length) {
			$(".toplevel_page_et_extra_options h1#epanel-title").html($(".toplevel_page_et_extra_options h1#epanel-title").html().replace("Extra Theme Options",dwd_custom_label_var.dwd_custom_divi_label_text + ' Theme Options'));
		}
  });
})( jQuery );
