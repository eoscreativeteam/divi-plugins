(function($) {
    'use strict';

    /**
     * All of the code for your admin-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
     *
     * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */

    $(window).on('pageshow', function() {
        function dwd_clear_localstorage() {
            localStorage.clear();
            //console.log('Cleared');
        }
        if ($('.et_pb_toggle_builder_wrapper.et_pb_builder_is_used').length) {
            $("#et_pb_fb_cta").after($('<a onclick="window.localStorage.clear();" id="dwd-clearstorage" class="button button-primary button-large" data-icon="" title="This will clear the localStorage on your browser. Use this when you are experiencing Quota Exceeded for copy and paste module.">Clear Divi localStorage</a>'));

        }
        $("#dwd-clearstorage").click(function() {
            $('hr.wp-header-end').after($('<div class="notice-success hidden notice is-dismissible " style="display: block;"><p>Divi localStorage cleared.</p></div>').fadeIn('normal').delay(1000).fadeOut('slow'));
        });


    });


})(jQuery);